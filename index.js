const path = require('path')
const express = require('express')

const app = express()
app.use(express.json())
app.use((req, res) => {
    let url = req.url

    if(path.extname(url) == '.json' || path.extname(url) == '.gz' || path.extname(url) == '.bytes'){
        filePath = path.join(__dirname, 'public', url)
    }else{
        filePath = path.join(__dirname, 'public', url, 'data.json')
    }

    res.sendFile(filePath, function(err){
        res.send(err)
        // if(err && err.code == 'ENOENT'){
        //     res.sendStatus(404)
        //     return
        // }else{
        //     res.sendStatus(400)
        // }
    });
})

const PORT = process.env.PORT || 3000
app.listen(PORT, () => console.log(`server started at port ${PORT}`))